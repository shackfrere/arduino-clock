#include <LiquidCrystal.h>

#define null 0

using namespace std;
class Menu;
const char* months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
const char* days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

bool incrementRange(int lower, int upper, int *value);
bool decrementRange(int lower, int upper, int *value);
unsigned long int getTimeDiff(unsigned long int left, unsigned long int right);
bool tick(long int drift);

class LcdBacklight {
    public:
    bool on;
    int pin;
    LcdBacklight(int);
    void init();
    void switchOn();
    void switchOff();
    void toggle();
};

class Clock;

class Light {
    public:
    bool on;
    int pin;
    int dutyCycleTotal;
    int dutyCycleOn;
    int dutyCyclePosition;
    Clock* now;
    Clock* switchOnTime;
    Clock* timeout;
    Light(int, Clock&, Clock&);
    void init();
    void switchOn();
    void switchOff();
    void toggle();
    void blink();
    void increaseBrightness();
    void resetBrightness();
    void switchOffOnTimeout();
};

class RotaryEncoder {
    public:
    int dataPin;
    int clkPin;
    int swPin;
    bool clkPinValue;
    bool swPinValue;
    unsigned long int clickedTimestamp;
    bool clickLong;
    bool debouncing;
    unsigned long int releasedTimestamp;
    RotaryEncoder(int, int, int);
    void init();
    bool readDataPin();
    bool readClkPin();
    bool readSwPin();
    bool pollRotation();
    bool pollClick();
};

class Clock {
    public:
    int h;
    int m;
    int s;
    int Y;
    int M;
    int D;
    int w;
    long int drift;
    Clock();
    void addSecond(bool);
    void addMinute(bool);
    void addHour(bool);
    void addDay(bool);
    void addMonth(bool);
    void addYear();
    void takeAwayMinute();
    void takeAwayHour();
    void takeAwayDay();
    void takeAwayMonth();
    void takeAwayYear();
    int getMonthLength();
    int getWeekDay();
    String toString();
    String timeToString();
    String dateToString();
    Clock operator+(Clock&);
    bool operator<(Clock&);
    bool operator==(Clock&);
    bool operator>(Clock&);
    bool operator>=(Clock&);
    bool operator<=(Clock&);
};


class Alarm : public Clock {
    public:
    Clock alarmDuration;
    bool enabled;
    bool ringing;
    bool snoozed;
    bool ringOnWeekend;
    bool lcdBacklightState;
    bool ringLcdBacklight;
    Light* light;
    LcdBacklight* lcdBacklight;
    Alarm(Light&, LcdBacklight&);
    bool shouldRing(Clock&);
    void startRinging();
    void stopRinging();
};

class Menu {
    public:
    bool selected;
    Menu *parent;
    Menu *child;
    Menu* next;
    Menu* prev;
    String title;
    LiquidCrystal* lcd;
    Menu(String, LiquidCrystal&);
    virtual Menu* select();
    virtual Menu* goNext();
    virtual Menu* goPrev();
    void setNext(Menu*);
    void setChild(Menu*);
    virtual void displayMenu();
};

class RootMenu : public Menu {
    public:
    Clock* clock;
    Alarm* alarm;
    RootMenu(String, LiquidCrystal&, Clock&, Alarm&);
    void displayMenu();
};

class LeafMenu : public Menu {
    public:
    LeafMenu(String, LiquidCrystal&);
    Menu* select();
};

class TimeMenu : public LeafMenu {
    public:
    Clock* clock;
    TimeMenu(String, LiquidCrystal&, Clock&);
    void displayMenu();
};

class DateMenu : public LeafMenu {
    public:
    Clock* clock;
    DateMenu(String, LiquidCrystal&, Clock&);
    void displayMenu();
};

class BackMenu : public Menu {
    public:
    BackMenu(String, LiquidCrystal&);
    Menu* select();
};

class HoursMenu : public TimeMenu {
    public:
    HoursMenu(String, LiquidCrystal&, Clock&);
    Menu* goNext();
    Menu* goPrev();
};

class MinutesMenu : public TimeMenu {
    public:
    MinutesMenu(String, LiquidCrystal&, Clock&);
    Menu* goNext();
    Menu* goPrev();
};

class MonthsMenu : public DateMenu {
    public:
    MonthsMenu(String, LiquidCrystal&, Clock&);
    Menu* goNext();
    Menu* goPrev();
};

class YearsMenu : public DateMenu {
    public:
    YearsMenu(String, LiquidCrystal&, Clock&);
    Menu* goNext();
    Menu* goPrev();
};

class DaysMenu : public DateMenu {
    public:
    DaysMenu(String, LiquidCrystal&, Clock&);
    Menu* goNext();
    Menu* goPrev();
};

class AlarmEnableMenu : public LeafMenu {
    public:
    Alarm *alarm;
    AlarmEnableMenu(String, LiquidCrystal&, Alarm&);
    Menu* goNext();
    Menu* goPrev();
    void displayMenu();
};

class AlarmRingWeekendMenu : public LeafMenu {
    public:
    Alarm *alarm;
    AlarmRingWeekendMenu(String, LiquidCrystal&, Alarm&);
    Menu* goNext();
    Menu* goPrev();
    void displayMenu();
};

class AlarmLcdBacklightMenu : public LeafMenu {
    public:
    Alarm *alarm;
    AlarmLcdBacklightMenu(String, LiquidCrystal&, Alarm&);
    Menu* goNext();
    Menu* goPrev();
    void displayMenu();
};

class LcdBacklightMenu : public LeafMenu {
    public:
    LcdBacklight *backlight;
    LcdBacklightMenu(String, LiquidCrystal&, LcdBacklight&);
    Menu* goNext();
    Menu* goPrev();
    void displayMenu();
};

class DriftMenu : public LeafMenu {
    public:
    Clock *clock;
    DriftMenu(String, LiquidCrystal&, Clock&);
    Menu* goNext();
    Menu* goPrev();
    void displayMenu();
};

LcdBacklight lcdBacklight(6);
Clock clock;
Clock lightTimout;
Light light(13, clock, lightTimout);
Alarm alarm(light, lcdBacklight);
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
Menu *menu;
RotaryEncoder encoder(4, 2, 3);
RootMenu r("Main", lcd, clock, alarm);
Menu t("Time", lcd);
Menu d("Date", lcd);
Menu a("Alarm", lcd);
LcdBacklightMenu l("Lcd Backlight", lcd, lcdBacklight);
Menu g("Light", lcd);
DriftMenu f("Drift", lcd, clock);
BackMenu b("Back", lcd);

HoursMenu th("Hours", lcd, clock);
MinutesMenu tm("Minutes", lcd, clock);
BackMenu tb("Back", lcd);

YearsMenu dy("Year", lcd, clock);
MonthsMenu dm("Month", lcd, clock);
DaysMenu dd("Date", lcd, clock);
BackMenu db("Back", lcd);

HoursMenu ah("Hours", lcd, alarm);
MinutesMenu am("Minutes", lcd, alarm);
AlarmEnableMenu at("Enabled", lcd, alarm);
AlarmRingWeekendMenu aw("Ring On Weekend", lcd, alarm);
AlarmLcdBacklightMenu al("Enable Backlight", lcd, alarm);
BackMenu ab("Back", lcd);

Menu gt("Timeout", lcd);
BackMenu gb("Back", lcd);

HoursMenu gth("Hours", lcd, lightTimout);
MinutesMenu gtm("Minutes", lcd, lightTimout);
BackMenu gtb("Back", lcd);

void setup() {
    encoder.init();
    light.init();
    lcd.begin(16, 2);
    lcdBacklight.init();
    r.setNext(&r);

    r.setChild(&t);
    t.setNext(&d);
    d.setNext(&a);
    a.setNext(&l);
    l.setNext(&g);
    g.setNext(&f);
    f.setNext(&b);
    b.setNext(&t);

    t.setChild(&th);
    th.setNext(&tm);
    tm.setNext(&tb);
    tb.setNext(&th);

    d.setChild(&dy);
    dy.setNext(&dm);
    dm.setNext(&dd);
    dd.setNext(&db);
    db.setNext(&dy);

    a.setChild(&ah);
    ah.setNext(&am);
    am.setNext(&at);
    at.setNext(&aw);
    aw.setNext(&al);
    al.setNext(&ab);
    ab.setNext(&ah);

    g.setChild(&gt);
    gt.setNext(&gb);
    gb.setNext(&gt);

    gt.setChild(&gth);
    gth.setNext(&gtm);
    gtm.setNext(&gtb);
    gtb.setNext(&gth);

    menu = &r;
}

void loop() {
    bool turned = encoder.pollRotation();
    bool clicked = encoder.pollClick();
    bool ticked = tick(clock.drift);

    if(clicked) {
        if(alarm.ringing) {
            alarm.stopRinging();
            alarm.snoozed = true;
        } else if(encoder.clickLong) {
            light.toggle();
        } else {
            menu = menu->select();
            menu->displayMenu();
        }
    }

    if(turned) {
        bool direction = encoder.readDataPin();
        if(direction == true) {
            menu = menu->goPrev();
            menu->displayMenu();
        } else {
            menu = menu->goNext();
            menu->displayMenu();
        }
    }

    if(ticked) {
        clock.addSecond(false);
        menu->displayMenu();
    }

    if(alarm.shouldRing(clock)) {
        if(ticked) {
            light.increaseBrightness();
        }
        light.blink();
        alarm.ringing = true;
    }

    light.switchOffOnTimeout();
}

bool incrementRange(int lower, int upper, int *value) {
    bool overflow = false;

    *value = *value + 1;
    if(*value > upper) {
        *value = lower;
        overflow = true;
    }
    return overflow;
}

bool decrementRange(int lower, int upper, int *value) {
    bool underflow = false;

    *value = *value - 1;
    if(*value < lower) {
        *value = upper;
        underflow = true;
    }
    return underflow;
}

unsigned long int getTimeDiff(unsigned long int left, unsigned long int right) {
    unsigned long int diff;
    if(left > right) {
        diff = right + (-left);
    } else {
        diff = right - left;
    }
    return diff;
}

bool tick(long int drift) {
    static unsigned long int before = micros();
    unsigned long int now = micros();
    unsigned long int diff = getTimeDiff(before, now);
    if(diff >= (unsigned long int)(1000000 + drift)) {
        before += (1000000 + drift);
        return true;
    } else {
        return false;
    }
}

LcdBacklight::LcdBacklight(int pin) {
    this->pin = pin;
    on = false;
}

void LcdBacklight::init() {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
}

void LcdBacklight::switchOn() {
    on = true;
    digitalWrite(pin, LOW);
}

void LcdBacklight::switchOff() {
    on = false;
    digitalWrite(pin, HIGH);
}

void LcdBacklight::toggle() {
    if(on) {
        switchOff();
    } else {
        switchOn();
    }
}

Light::Light(int pin, Clock& now, Clock& timeout) {
    this->pin = pin;
    this->now = &now;
    on = false;
    dutyCycleTotal = 300;
    dutyCyclePosition = 0;
    dutyCycleOn = 1;
    switchOnTime = null;
    this->timeout = &timeout;
    this->timeout->h = 1;
    this->timeout->Y = 0;
}

void Light::init() {
    pinMode(pin, OUTPUT);
}

void Light::switchOn() {
    on = true;
    if(switchOnTime == null) {
        switchOnTime = new Clock(*now);
    }
    digitalWrite(pin, HIGH);
}

void Light::switchOff() {
    on = false;
    if(switchOnTime != null) {
        delete(switchOnTime);
        switchOnTime = null;
    }
    digitalWrite(pin, LOW);
}

void Light::blink() {
    dutyCyclePosition = (dutyCyclePosition + 1) % dutyCycleTotal;
    if(dutyCyclePosition <= dutyCycleOn) {
        switchOn();
    } else {
        switchOff();
    }
}

void Light::toggle() {
    if(on) {
        switchOff();
        return;
    }
    switchOn();
}

void Light::increaseBrightness() {
    dutyCycleOn++;
    if(dutyCycleOn >= dutyCycleTotal) {
        dutyCycleOn = dutyCycleTotal;
    }
}

void Light::resetBrightness() {
    dutyCycleOn = 1;
}

void Light::switchOffOnTimeout() {
    if(on) {
        if((*switchOnTime + *timeout) <= *now) {
            switchOff();
        }
    }
}

RotaryEncoder::RotaryEncoder(int dataPin, int clkPin, int swPin) {
    this->dataPin = dataPin;
    this->clkPin = clkPin;
    this->swPin = swPin;
    this->clkPinValue = false;
    this->swPinValue = false;
    clickedTimestamp = 0;
    clickLong = false;
    releasedTimestamp = 0;
    debouncing = false;
}

void RotaryEncoder::init() {
    pinMode(this->dataPin, INPUT);
    pinMode(this->clkPin, INPUT);
    pinMode(this->swPin, INPUT);
}

bool RotaryEncoder::readDataPin() {
    return !digitalRead(this->dataPin);
}

bool RotaryEncoder::readClkPin() {
    return !digitalRead(this->clkPin);
}

bool RotaryEncoder::readSwPin() {
    return !digitalRead(this->swPin);
}

bool RotaryEncoder::pollRotation() {
    bool current = readClkPin();
    if(current == false) {
        this->clkPinValue = false;
        return false;
    }

    if(this->clkPinValue == false) {
        this->clkPinValue = true;
        return true;
    }

    return false;
}

bool RotaryEncoder::pollClick() {
    unsigned long int now = millis();
    unsigned long int diff;

    if(debouncing) {
        diff = getTimeDiff(this->releasedTimestamp, now);
        if(diff > 50) {
            debouncing = false;
        }
        return false;
    }

    bool oldValue = swPinValue;
    swPinValue = readSwPin();

    if((oldValue == false) && (swPinValue == true)) {
        clickedTimestamp = now;
        return false;
    }

    diff = getTimeDiff(this->clickedTimestamp, now);
    if((oldValue == true) && (swPinValue == false)) {
        debouncing = true;
        releasedTimestamp = now;
        if(diff > 500) {
            clickLong = true;
        } else {
            clickLong = false;
        }
        return true;
    }

    return false;
}

Clock::Clock() {
    h = 0;
    m = 0;
    s = 0;
    Y = 2019;
    M = 0;
    D = 0;
    drift = 0;
}

void Clock::addSecond(bool isolated) {
    bool overflow;
    overflow = incrementRange(0, 59, &(this->s));
    if(!isolated && overflow) {
        this->addMinute(isolated);
    }
}

void Clock::addMinute(bool isolated) {
    bool overflow;
    overflow = incrementRange(0, 59, &(this->m));
    if(!isolated && overflow) {
        this->addHour(isolated);
    }
}

void Clock::addHour(bool isolated) {
    bool overflow;
    overflow = incrementRange(0, 23, &(this->h));
    if(!isolated && overflow) {
        this->addDay(isolated);
    }
}

void Clock::addDay(bool isolated) {
    bool overflow;
    overflow = incrementRange(0, this->getMonthLength() - 1, &(this->D));
    if(!isolated && overflow) {
        this->addMonth(isolated);
    }
}

void Clock::addMonth(bool isolated) {
    bool overflow;
    overflow = incrementRange(0, 11, &(this->M));
    if(!isolated && overflow) {
        this->addYear();
    }
}

void Clock::addYear() {
    this->Y = this->Y + 1;
}

void Clock::takeAwayMinute() {
    decrementRange(0, 59, &(this->m));
}

void Clock::takeAwayHour() {
    decrementRange(0, 23, &(this->h));
}

void Clock::takeAwayYear() {
    this->Y = this->Y - 1;
}

void Clock::takeAwayMonth() {
    decrementRange(0, 11, &(this->M));
}

void Clock::takeAwayDay() {
    decrementRange(0, this->getMonthLength() - 1, &(this->D));
}

int Clock::getMonthLength() {
  int days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  if(this->M == 1) {
    if(((this->Y % 4) == 0) || ((this->Y % 100) == 0)) {
      return 29;
    }
  }
  return days[this->M];
}

int Clock::getWeekDay() {
    int y = this->Y;
    int m = this->M + 1;
    int d = this->D + 1;
    static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    y -= m < 3;
    return (y + y/4 - y/100 + y/400 + t[m-1] + d - 1) % 7;
}

String Clock::timeToString() {
    String date("");
    date += String(this->h);
    date += String(":");
    if(this->m < 10) {
        date += String("0");
    }
    date += String(this->m);
    return date;
}

String Clock::dateToString() {
    String date("");
    date += String(this->D + 1);
    date += String("/");
    date += String(this->M + 1);
    date += String("/");
    date += String(this->Y);
    return date;
}

Clock Clock::operator+(Clock& other) {
    Clock retval = Clock(*this);
    for(int i=0; i<other.s; i++) retval.addSecond(false);
    for(int i=0; i<other.m; i++) retval.addMinute(false);
    for(int i=0; i<other.h; i++) retval.addHour(false);
    for(int i=0; i<other.D; i++) retval.addDay(false);
    for(int i=0; i<other.M; i++) retval.addMonth(false);
    for(int i=0; i<other.Y; i++) retval.addYear();
    return retval;
}

bool Clock::operator<(Clock& other) {
    if(other.Y > this->Y) return true;
    if(other.Y < this->Y) return false;
    if(other.M > this->M) return true;
    if(other.M < this->M) return false;
    if(other.D > this->D) return true;
    if(other.D < this->D) return false;
    if(other.h > this->h) return true;
    if(other.h < this->h) return false;
    if(other.m > this->m) return true;
    if(other.m < this->m) return false;
    if(other.s > this->s) return true;
    if(other.s < this->s) return false;
    return false;
}

bool Clock::operator==(Clock& other) {
    if(other.Y != this->Y) return false;
    if(other.M != this->M) return false;
    if(other.D != this->D) return false;
    if(other.h != this->h) return false;
    if(other.m != this->m) return false;
    if(other.s != this->s) return false;
    return true;
}

bool Clock::operator>(Clock& other) {
    if(*this < other) return false;
    if(*this == other) return false;
    return true;
}

bool Clock::operator>=(Clock& other) {
    return ((*this > other) || (*this == other));
}

bool Clock::operator<=(Clock& other) {
    return ((*this < other) || (*this == other));
}

Alarm::Alarm(Light &light, LcdBacklight &lcdBacklight) {
    this->enabled = false;
    this->ringing = false;
    this->h = 0;
    this->m = 0;
    this->s = 0;
    this->Y = 0;
    this->M = 0;
    this->D = 0;
    alarmDuration = Clock();
    alarmDuration.m = 30;
    alarmDuration.Y = 0;
    this->light = &light;
    this->lcdBacklight = &lcdBacklight;
    lcdBacklightState = lcdBacklight.on;
    snoozed = false;
    ringOnWeekend = false;
    ringLcdBacklight = false;
}

bool Alarm::shouldRing(Clock& other) {
    if(!enabled) {
        return false;
    }

    if(!ringOnWeekend) {
        if(other.getWeekDay() >= 5) {
            return false;
        }
    }

    Clock clock = other;
    Clock stopTime = *this + this->alarmDuration;
    clock.Y = 0;
    clock.M = 0;
    clock.D = 0;
    bool timeToRing = (clock >= (Clock&)*this) && (clock < stopTime);

    if(timeToRing) {
        if(snoozed) {
            return false;
        }

        if(!ringing) {
            startRinging();
        }

        return true;
    } else {
        if(ringing) {
            stopRinging();
        }

        snoozed = false;
        return false;
    }
}

void Alarm::startRinging() {
    if(ringLcdBacklight) {
        lcdBacklightState = lcdBacklight->on;
        if(!lcdBacklightState) {
            lcdBacklight->switchOn();
        }
    }
    ringing = true;
}

void Alarm::stopRinging() {
    if(ringLcdBacklight) {
        if(!lcdBacklightState) {
            lcdBacklight->switchOff();
        }
    }
    ringing = false;
    light->switchOff();
    light->resetBrightness();
}

Menu::Menu(String title, LiquidCrystal& lcd) {
    this->lcd = &lcd;
    selected = false;
    this->title = title;
}

Menu* Menu::select() {
    this->lcd->setCursor(0, 1);
    this->lcd->write("            ");
    return this->child;
}

Menu* Menu::goNext() {
    this->lcd->setCursor(0, 1);
    this->lcd->write("            ");
    return this->next;
}

Menu* Menu::goPrev() {
    this->lcd->setCursor(0, 1);
    this->lcd->write("            ");
    return this->prev;
}

void Menu::setNext(Menu *menu) {
    this->next = menu;
    menu->prev = this;
    menu->parent = this->parent;
}

void Menu::setChild(Menu *menu) {
    this->child = menu;
    menu->parent = this;
}

void Menu::displayMenu() {
    lcd->clear();
    lcd->setCursor(0, 0);
    lcd->print(this->title);
}

RootMenu::RootMenu(String title, LiquidCrystal& lcd, Clock& clock, Alarm& alarm) : Menu(title, lcd) {
    this->clock = &clock;
    this->alarm = &alarm;
}

void RootMenu::displayMenu() {
    lcd->clear();
    lcd->setCursor(6, 0);
    lcd->print(clock->timeToString());
    if(alarm->enabled) {
        lcd->setCursor(15, 0);
        lcd->print("*");
    }
    lcd->setCursor(0, 1);
    lcd->print(days[clock->getWeekDay()]);
    int length = clock->dateToString().length();
    lcd->setCursor(16 - length, 1);
    lcd->print(clock->dateToString());
}

LeafMenu::LeafMenu(String title, LiquidCrystal& lcd) : Menu(title, lcd) {
}

Menu* LeafMenu::select() {
    if(selected) {
        selected = false;
    } else {
        selected = true;
    }
    return this;
}

TimeMenu::TimeMenu(String title, LiquidCrystal& lcd, Clock& clock) : LeafMenu(title, lcd) {
    this->clock = &clock;
}

void TimeMenu::displayMenu() {
    Menu::displayMenu();
    lcd->setCursor(0, 1);
    lcd->print(clock->timeToString());
}

DateMenu::DateMenu(String title, LiquidCrystal& lcd, Clock& clock) : LeafMenu(title, lcd) {
    this->clock = &clock;
}

void DateMenu::displayMenu() {
    Menu::displayMenu();
    lcd->setCursor(0, 1);
    lcd->print(clock->dateToString());
}

BackMenu::BackMenu(String title, LiquidCrystal& lcd) : Menu(title, lcd) {
}

Menu* BackMenu::select() {
    return this->parent;
}

HoursMenu::HoursMenu(String title, LiquidCrystal& lcd, Clock& clock) : TimeMenu(title, lcd, clock) {
}

Menu* HoursMenu::goNext() {
    if(this->selected) {
        this->clock->addHour(true);
        return this;
    }
    return this->next;
}

Menu* HoursMenu::goPrev() {
    if(this->selected) {
        this->clock->takeAwayHour();
        return this;
    }
    return this->prev;
}

MinutesMenu::MinutesMenu(String title, LiquidCrystal& lcd, Clock& clock) : TimeMenu(title, lcd, clock) {
}

Menu* MinutesMenu::goNext() {
    if(this->selected) {
        this->clock->addMinute(true);
        return this;
    }
    return this->next;
}

Menu* MinutesMenu::goPrev() {
    if(this->selected) {
        this->clock->takeAwayMinute();
        return this;
    }
    return this->prev;
}

YearsMenu::YearsMenu(String title, LiquidCrystal& lcd, Clock& clock) : DateMenu(title, lcd, clock) {
}

Menu* YearsMenu::goNext() {
    if(this->selected) {
        this->clock->addYear();
        return this;
    }
    return this->next;
}

Menu* YearsMenu::goPrev() {
    if(this->selected) {
        this->clock->takeAwayYear();
        return this;
    }
    return this->prev;
}

MonthsMenu::MonthsMenu(String title, LiquidCrystal& lcd, Clock& clock) : DateMenu(title, lcd, clock) {
}

Menu* MonthsMenu::goNext() {
    if(this->selected) {
        this->clock->addMonth(true);
        return this;
    }
    return this->next;
}

Menu* MonthsMenu::goPrev() {
    if(this->selected) {
        this->clock->takeAwayMonth();
        return this;
    }
    return this->prev;
}

DaysMenu::DaysMenu(String title, LiquidCrystal& lcd, Clock& clock) : DateMenu(title, lcd, clock) {
}

Menu* DaysMenu::goNext() {
    if(this->selected) {
        this->clock->addDay(true);
        return this;
    }
    return this->next;
}

Menu* DaysMenu::goPrev() {
    if(this->selected) {
        this->clock->takeAwayDay();
        return this;
    }
    return this->prev;
}

AlarmEnableMenu::AlarmEnableMenu(String title, LiquidCrystal& lcd, Alarm& alarm) : LeafMenu(title, lcd) {
    this->alarm = &alarm;
}

Menu* AlarmEnableMenu::goNext() {
    if(selected) {
        alarm->enabled = !alarm->enabled;
        return this;
    }
    return this->next;
}

Menu* AlarmEnableMenu::goPrev() {
    if(selected) {
        alarm->enabled = !alarm->enabled;
        return this;
    }
    return this->prev;
}

void AlarmEnableMenu::displayMenu() {
    LeafMenu::displayMenu();
    
    lcd->setCursor(0, 1);
    if(alarm->enabled) {
        lcd->print("Yes");
    } else {
        lcd->print("No");
    }
}

AlarmRingWeekendMenu::AlarmRingWeekendMenu(String title, LiquidCrystal& lcd, Alarm& alarm) : LeafMenu(title, lcd) {
    this->alarm = &alarm;
}

Menu* AlarmRingWeekendMenu::goNext() {
    if(selected) {
        alarm->ringOnWeekend = !alarm->ringOnWeekend;
        return this;
    }
    return this->next;
}

Menu* AlarmRingWeekendMenu::goPrev() {
    if(selected) {
        alarm->ringOnWeekend = !alarm->ringOnWeekend;
        return this;
    }
    return this->prev;
}

void AlarmRingWeekendMenu::displayMenu() {
    LeafMenu::displayMenu();
    
    lcd->setCursor(0, 1);
    if(alarm->ringOnWeekend) {
        lcd->print("Yes");
    } else {
        lcd->print("No");
    }
}

AlarmLcdBacklightMenu::AlarmLcdBacklightMenu(String title, LiquidCrystal& lcd, Alarm& alarm) : LeafMenu(title, lcd) {
    this->alarm = &alarm;
}

Menu* AlarmLcdBacklightMenu::goNext() {
    if(selected) {
        alarm->ringLcdBacklight = !alarm->ringLcdBacklight;
        return this;
    }
    return this->next;
}

Menu* AlarmLcdBacklightMenu::goPrev() {
    if(selected) {
        alarm->ringLcdBacklight = !alarm->ringLcdBacklight;
        return this;
    }
    return this->prev;
}

void AlarmLcdBacklightMenu::displayMenu() {
    LeafMenu::displayMenu();
    
    lcd->setCursor(0, 1);
    if(alarm->ringLcdBacklight) {
        lcd->print("Yes");
    } else {
        lcd->print("No");
    }
}

LcdBacklightMenu::LcdBacklightMenu(String title, LiquidCrystal& lcd, LcdBacklight& backlight) : LeafMenu(title, lcd) {
    this->backlight = &backlight;
}

Menu* LcdBacklightMenu::goNext() {
    if(selected) {
        backlight->toggle();
        return this;
    }
    return this->next;
}

Menu* LcdBacklightMenu::goPrev() {
    if(selected) {
        backlight->toggle();
        return this;
    }
    return this->prev;
}

void LcdBacklightMenu::displayMenu() {
    LeafMenu::displayMenu();
    
    lcd->setCursor(0, 1);
    if(backlight->on) {
        lcd->print("Yes");
    } else {
        lcd->print("No");
    }
}

DriftMenu::DriftMenu(String title, LiquidCrystal& lcd, Clock& clock) : LeafMenu(title, lcd) {
    this->clock = &clock;
}

Menu* DriftMenu::goNext() {
    if(selected) {
        clock->drift = clock->drift + 10;
        return this;
    }
    return this->next;
}

Menu* DriftMenu::goPrev() {
    if(selected) {
        clock->drift = clock->drift - 10;
        return this;
    }
    return this->prev;
}

void DriftMenu::displayMenu() {
    LeafMenu::displayMenu();
    
    lcd->setCursor(0, 1);
    lcd->print(clock->drift);
}
